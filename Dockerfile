FROM openjdk:8-alpine

ARG value1=${x}
ARG value2=${y}

ENV H2=${value1}
ENV JAR=${value2}

WORKDIR /app
COPY ./target/*.jar  /app      

EXPOSE 8090
USER root 
RUN adduser -D sara && chmod 777 -R /app
USER sara
CMD java -jar ${H2} ${JAR}
